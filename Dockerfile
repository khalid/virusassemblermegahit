FROM mbbteam/mbb_workflows_base:latest as alltools

RUN apt -y update && apt install -y fastqc=0.11.5+dfsg-6

RUN pip3 install cutadapt==2.3

RUN apt-get install -y pigz

RUN cd /opt/biotools \
 && wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip \
 && unzip Trimmomatic-0.38.zip \
 && echo -e '#!/bin/bash java -jar /opt/biotools/Trimmomatic-0.38/trimmomatic-0.38.jar' > bin/trimmomatic \
 && chmod 777 bin/trimmomatic \
 && rm Trimmomatic-0.38.zip

RUN apt -y update && apt install -y openjdk-8-jre

RUN cd /opt/biotools \
 && git clone https://github.com/voutcn/megahit.git \
 && cd megahit \
 && git submodule update --init \
 && mkdir build && cd build \
 && cmake .. -DCMAKE_BUILD_TYPE=Release \
 && make -j8 \
 && make simple_test \
 && make install \
 && cd /opt/biotools \
 && rm -rf megahit

ENV PATH $PATH:/opt/biotools/ncbi-blast-2.10.1+/bin
RUN cd /opt/biotools/ \
 && wget -O ncbi-blast-2.10.1+.tar.gz https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.10.1+-x64-linux.tar.gz \
 && tar -xvzf ncbi-blast-2.10.1+.tar.gz

RUN cd /opt/biotools \
 && git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git \
 && cd mbb_mqc_plugin \
 && python3 setup.py install

RUN cd /opt/biotools \
 && wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2 \
 && tar -xvjf bwa-0.7.17.tar.bz2 \
 && cd bwa-0.7.17 \
 && make -j 10 \
 && mv bwa ../bin/ \
 && cd .. \
 && rm -r bwa-0.7.17 bwa-0.7.17.tar.bz2

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY ./files/data/blast_db_virus /blast_db_virus
COPY files /workflow
COPY sagApp /sagApp

