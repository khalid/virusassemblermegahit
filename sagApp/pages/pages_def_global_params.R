tabglobal_params = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectglobal_params", label = "", value="global_params")),box(title = "Global parameters :", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		tags$label("Results directory: "),
		fluidRow(
			column(4,shinyDirButton("shinydir_results_dir",label="Please select a directory", title="Results directory: ")),
			column(8,textInput("results_dir",label=NULL,value=""))
		)
,

		tags$label("Directory containing the fastq files: "),
		fluidRow(
			column(4,shinyDirButton("shinydir_sample_dir",label="Please select a directory", title="Directory containing the fastq files: ")),
			column(8,textInput("sample_dir",label=NULL,value=""))
		)
,

		radioButtons("SeOrPe", label = "Single end reads (SE) or Paired end reads (PE): ", choices = list("Single end" = "SE", "Paired end" = "PE"),  selected = "PE", width =  "auto"),

		textAreaInput("memo", label = "Text area for the user", value = "")

	)))


